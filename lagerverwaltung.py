from enum import Enum
import csv


class SortType(Enum):
    ASC = 'asc'
    DESC = 'desc'


class Artikel:
    def __init__(self, nummer, bezeichnung, bestand, preis):
        self.nummer = nummer
        self.bezeichnung = bezeichnung
        self.bestand = bestand
        self.preis = preis


class Lagerverwaltung:
    """ Stellt eine Lagerverwaltung mit Artikeln dar"""

    artikel = []

    def add_artikel(self, nummer, bezeichnung, bestand, preis):
        """ Fügt einen Artikel der Lagerverwaltung hinzu. """
        self.artikel.append(Artikel(nummer, bezeichnung, bestand, preis))

    def remove_arikel(self, nummer):
        self.artikel = [a for a in self.artikel if a.nummer != nummer]

    def sort(self, attr, sorttype):
        if sorttype == SortType.ASC:
            self.artikel.sort(lambda art: art[attr], reverse=False)

        if sorttype == SortType.DESC:
            self.artikel.sort(key=lambda art: getattr(art, attr.lower()), reverse=True)

    def save_to_csv(self, path):
        """ Speichert alle Artikel in einer CSV Datei. """
        with open(path, 'w') as file:
            csvwriter = csv.writer(file)

            for artikel in self.artikel:
                csvwriter.writerow([
                    artikel.nummer,
                    artikel.bezeichnung,
                    artikel.bestand,
                    artikel.preis,
                ])

    def read_from_csv(self, path):
        """ Liest Artikel aus einer CSV Satei """
        with open(path, 'r') as file:
            csvreader = csv.reader(file)
            for row in csvreader:
                self.artikel.append(
                    Artikel(row[0], row[1], int(row[2]), row[3]))
