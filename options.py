from lagerverwaltung import SortType


def close(lagerverwaltung):
    quit()


def read(lagerverwaltung):
    lagerverwaltung.read_from_csv('daten.csv')


def save(lagerverwaltung):
    lagerverwaltung.save_to_csv('daten.csv')


def edit(lagerverwaltung):
    print('Bitte die nummer des Artikels eingeben.')
    nummer = input()
    for artikel in lagerverwaltung.artikel:
        if artikel.nummer == nummer:
            print('Bezeichnung (Leer = ' + artikel.bezeichnung + ') :')
            artikel.bezeichnung = input().strip() or artikel.bezeichnung
            print('Bestand (Leer = ' + str(artikel.bestand) + ') :')
            artikel.bestand = int(input().strip() or artikel.bestand)
            print('Preis (Leer = ' + artikel.preis + ') :')
            artikel.preis = input().strip() or artikel.preis


def print_one(lagerverwaltung):
    print('Bitte die Nummer des Artikels eingeben.')
    nummer = input()
    for artikel in lagerverwaltung.artikel:
        if artikel.nummer == nummer:
            print(
                "Nummer: " + artikel.nummer + "; " +
                "Bezeichnung: " + artikel.bezeichnung + "; " +
                "Bestand: " + str(artikel.bestand) + "; " +
                "Preis: " + artikel.preis
            )

def delete_all(lagerverwaltung):
    lagerverwaltung.artikel = []

def delete_one(lagerverwaltung):
    print('Bitte die Nummer des Artikels eingeben.')
    nummer = input()
    lagerverwaltung.remove_arikel(nummer)

def print_all(lagerverwaltung):
    for artikel in lagerverwaltung.artikel:
        print(
            "Nummer: " + artikel.nummer + "; " +
            "Bezeichnung: " + artikel.bezeichnung + "; " +
            "Bestand: " + str(artikel.bestand) + "; " +
            "Preis: " + artikel.preis
        )


def add_artikel(lagerverwaltung):
    print("Nummer: ")
    nummer = input()
    print("Bezeichnung: ")
    bezeichnung = input()
    print("Bestand: ")
    bestand = int(input())
    print("Preis: ")
    preis = input()

    lagerverwaltung.add_artikel(nummer, bezeichnung, bestand, preis)


def sort(lagerverwaltung):
    print("Wonach soll sortiert werden?")
    attr = input()
    print("0: Aufsteigend; 1: Absteigend")
    x = int(input())

    if x == 0:
        lagerverwaltung.sort(attr, SortType.ASC)
    elif x == 1:
        lagerverwaltung.sort(attr, SortType.DESC)
    else:
        print("Bitte geben Sie einen validen Wert ein.")
