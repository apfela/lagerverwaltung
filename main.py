from lagerverwaltung import Lagerverwaltung
import options

lagerverwaltung = Lagerverwaltung()

OPTIONS = [
    options.read,
    options.save,
    options.print_all,
    options.add_artikel,
    options.edit,
    options.delete_one,
    options.delete_all,
    options.sort,
    options.close,
]

while True:
    print("-- Menu --")
    print("1: Aus CSV Lesen")
    print("2: In CSV Speichern")
    print("3: Alle Artikel ausgeben")
    print("4: Artikel hinzufügen")
    print("5: Editieren")
    print("6: Artikel löschen")
    print("7: Alle Artikel löschen")
    print("8: Sortieren")
    print("9: Schließen")
    SELECTED_OPTION = int(input())

    if SELECTED_OPTION > 0 and SELECTED_OPTION <= len(OPTIONS):
        OPTIONS[SELECTED_OPTION - 1](lagerverwaltung)
    else:
        print('Diese Option existiert nicht.')
